import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { IoIosArrowDown } from "react-icons/io";
import { toggleProgressList } from "../redux/quiz/quizActions";

function ProgressList() {
  const showProgressList = useSelector((state) => state.quiz.showProgressList);
  const quizStarted = useSelector((state) => state.quiz.quizStarted);
  const questionNumber = useSelector((state) => state.quiz.quizQuestionNumber);
  const gameOver = useSelector((state) => state.quiz.gameOver);
  const dispatch = useDispatch();

  let questions = [];
  for (let i = 0; i < 15; i++) {
    questions.push(
      <p key={i} className={`${gameOver && questionNumber === i + 2 ? "earned" : !gameOver && questionNumber === i + 1 ? "current" : ""}`}>
        <span>{i + 1}</span>
        <span>${1000 * Math.pow(i + 1, 2)}</span>
      </p>
    );
  }

  return (
    <div
      className={`progress-list-page ${!quizStarted ? "hidden" : !showProgressList ? "mobile-hidden" : ""}`}
      onClick={() => dispatch(toggleProgressList())}
    >
      <div className="question-numbers">{questions.reverse()}</div>
      <IoIosArrowDown size={26} className={`${!quizStarted ? "hide" : ""}`} />
    </div>
  );
}

export default ProgressList;
