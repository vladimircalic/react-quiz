import React from "react";
import QuestionContainer from "./QuestionContainer";
import GameOver from "./GameOver";
import BestResults from "./BestResults";
import { useSelector, useDispatch } from "react-redux";
import { startNewQuiz, getQuestion } from "../redux/quiz/quizActions";
import { GiMoneyStack } from "react-icons/gi";

function Menu() {
  const quizStarted = useSelector((state) => state.quiz.quizStarted);
  const loading = useSelector((state) => state.quiz.loading);
  const showBestResults = useSelector((state) => state.bestResults.showBestResults);
  const dispatch = useDispatch();

  const startQuiz = () => {
    dispatch(startNewQuiz());
    dispatch(getQuestion(1));
  };

  return (
    <div className="menu-page">
      {loading ? (
        <div className="loading">Loading...</div>
      ) : !quizStarted ? (
        <>
          <div className="body-header">
            <p>Test your knowledge with</p>
            <div className="main-header">
              <GiMoneyStack size={40} />
              <h2>REACT QUIZ</h2>
              <GiMoneyStack size={40} />
            </div>
            <p>How far can you get?</p>
          </div>
          <button onClick={() => startQuiz()} className="button start-quiz-button">
            Start Quiz
          </button>
        </>
      ) : (
        <QuestionContainer />
      )}
      <GameOver />
      {showBestResults ? <BestResults /> : ""}
    </div>
  );
}

export default Menu;
