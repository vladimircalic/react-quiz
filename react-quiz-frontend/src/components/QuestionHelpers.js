import React from "react";
import { useSelector, useDispatch } from "react-redux";
import helper1 from "../assets/helper-1.png";
import helper2 from "../assets/helper-2.png";
import helper3 from "../assets/helper-3.png";
import cross from "../assets/cross.png";
import { callFriendHelper, fiftyFiftyHelper, switchQuestionHelper } from "../redux/quiz/quizActions";

function QuestionHelps() {
  const quizStarted = useSelector((state) => state.quiz.quizStarted);
  const callFriend = useSelector((state) => state.quiz.questionHelpers.callFriend);
  const fiftyFifty = useSelector((state) => state.quiz.questionHelpers.fiftyFifty);
  const switchQuestion = useSelector((state) => state.quiz.questionHelpers.switchQuestion);
  const questionNumber = useSelector((state) => state.quiz.quizQuestionNumber);
  const activeQuestion = useSelector((state) => state.quiz.question);
  const gameOver = useSelector((state) => state.quiz.gameOver);
  const quizCompleted = useSelector((state) => state.quiz.quizCompleted);
  const dispatch = useDispatch();

  const handleFiftyFiftyHelper = () => {
    if (fiftyFifty && !gameOver && !quizCompleted) {
      const filteredOptions = [
        activeQuestion.answer,
        activeQuestion.options.filter((option) => {
          return option !== activeQuestion.answer;
        })[Math.floor(Math.random() * (activeQuestion.options.length - 1))],
      ];
      dispatch(fiftyFiftyHelper(filteredOptions));
    }
  };

  const handleCallFriendHelper = () => {
    if (callFriend && !gameOver && !quizCompleted) {
      let friendsAnswer;

      if (questionNumber <= 5)
        friendsAnswer = `I am ${Math.floor(Math.random() * (100 - 70)) + 70}% sure the answer is ${activeQuestion.answer}.`;
      else if (questionNumber > 5 && questionNumber <= 10) {
        const friendsOptions = [
          activeQuestion.answer,
          activeQuestion.options.filter((option) => {
            return option !== activeQuestion.answer;
          })[Math.floor(Math.random() * (activeQuestion.options.length - 1))],
        ];
        friendsAnswer = `Hmmm... I'm ${Math.floor(Math.random() * (60 - 40)) + 40}% sure its ${
          friendsOptions[Math.floor(Math.random() * friendsOptions.length)]
        }.`;
      } else
        friendsAnswer = `That's a hard one... My guess would be ${
          activeQuestion.options[Math.floor(Math.random() * activeQuestion.options.length)]
        }, but im only ${Math.floor(Math.random() * (30 - 20)) + 20}% sure.`;

      dispatch(callFriendHelper(friendsAnswer));
    }
  };

  const handleSwitchQuestionHelper = () => {
    if (switchQuestion && !gameOver && !quizCompleted) dispatch(switchQuestionHelper(activeQuestion));
  };

  return (
    <div className={`question-helps-page ${!quizStarted ? "hidden" : ""}`}>
      <div className="question-help">
        <img src={helper1} alt="helper 50:50" onClick={() => handleFiftyFiftyHelper()} />
        {!fiftyFifty ? <img src={cross} className="helper-cross" alt="helper switch question" /> : ""}
      </div>
      <div className="question-help">
        <img src={helper2} alt="helper call friend" onClick={() => handleCallFriendHelper()} />
        {!callFriend ? <img src={cross} className="helper-cross" alt="helper switch question" /> : ""}
      </div>
      <div className="question-help">
        <img src={helper3} alt="helper switch question" onClick={() => handleSwitchQuestionHelper()} />
        {!switchQuestion ? <img src={cross} className="helper-cross" alt="helper switch question" /> : ""}
      </div>
    </div>
  );
}

export default QuestionHelps;
