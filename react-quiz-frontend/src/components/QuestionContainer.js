import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getQuestion, gameOver, handleMoneyEarned, quizCompleted } from "../redux/quiz/quizActions";
import { checkHighScore } from "../redux/bestResults/bestResultsActions";
import { FaRegWindowClose } from "react-icons/fa";

function QuestionContainer() {
  const [confirmAnswerWindow, setConfirmAnswerWindow] = useState(false);
  const [selectedAnswer, setSelectedAnswer] = useState("");
  const [showFriendsAnswer, setShowFriendsAnswer] = useState(false);
  const [showAnswer, setShowAnswer] = useState(false);
  const quizStarted = useSelector((state) => state.quiz.quizStarted);
  const activeQuestion = useSelector((state) => state.quiz.question);
  const questionNumber = useSelector((state) => state.quiz.quizQuestionNumber);
  const filteredOptions = useSelector((state) => state.quiz.question.filteredOptions);
  const friendsAnswer = useSelector((state) => state.quiz.question.friendsAnswer);
  const dispatch = useDispatch();

  const showConfirmAnswerWindow = (option) => {
    setConfirmAnswerWindow(true);
    setSelectedAnswer(option);
  };

  const cancelConfirmAnswer = () => {
    setConfirmAnswerWindow(false);
    setSelectedAnswer("");
  };

  const revealAnswer = () => {
    setConfirmAnswerWindow(false);
    setShowAnswer(true);
  };

  const handleConfirmAnswer = () => {
    if (selectedAnswer === activeQuestion.answer) {
      revealAnswer();
      dispatch(handleMoneyEarned(1000 * Math.pow(questionNumber, 2)));
      setTimeout(() => {
        if (selectedAnswer === activeQuestion.answer && questionNumber === 15) {
          dispatch(quizCompleted());
          dispatch(checkHighScore(questionNumber));
        } else {
          setShowAnswer(false);
          dispatch(getQuestion(questionNumber + 1));
        }
      }, 2000);
    } else {
      revealAnswer();
      setTimeout(() => {
        dispatch(gameOver());
        dispatch(checkHighScore(questionNumber - 1));
      }, 2000);
    }
  };

  useEffect(() => {
    if (friendsAnswer) {
      setShowFriendsAnswer(true);
      // setTimeout(() => {
      //   setShowFriendsAnswer(false);
      // }, 10000);
    } else setShowFriendsAnswer(false);
  }, [friendsAnswer]);

  const closeFriendsAnswer = () => {
    setShowFriendsAnswer(false);
  };

  return (
    <div className={`question-page ${!quizStarted ? "hidden" : ""}`}>
      <div className="question">
        <h1>{activeQuestion.question}</h1>
      </div>
      <div className={`question-options ${showAnswer || confirmAnswerWindow ? "no-events" : ""}`}>
        {activeQuestion.options
          ? activeQuestion.options.map((option, index) => {
              return (
                <button
                  key={index}
                  className={`option button ${
                    filteredOptions && !filteredOptions.includes(option)
                      ? "filtered"
                      : activeQuestion.answer === option && showAnswer
                      ? "correct"
                      : selectedAnswer === option && showAnswer
                      ? "false"
                      : selectedAnswer === option
                      ? "active"
                      : ""
                  }`}
                  onClick={() => showConfirmAnswerWindow(option)}
                >
                  {option}
                </button>
              );
            })
          : ""}
      </div>
      <div className={`confirm-answer ${!confirmAnswerWindow ? "hidden" : ""}`}>
        <p>Is {selectedAnswer} your final answer?</p>
        <button className="button" onClick={() => handleConfirmAnswer()}>
          Yes
        </button>
        <button className="button" onClick={() => cancelConfirmAnswer()}>
          No
        </button>
      </div>
      {showFriendsAnswer ? (
        <div className="friends-answer">
          <FaRegWindowClose size={20} onClick={() => closeFriendsAnswer()} />
          <h2>Friend: {friendsAnswer}</h2>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export default QuestionContainer;
