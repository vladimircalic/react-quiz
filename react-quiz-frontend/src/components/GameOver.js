import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { endQuiz } from "../redux/quiz/quizActions";
import { getBestResults, toggleBestResults, saveUserScore } from "../redux/bestResults/bestResultsActions";

function GameOver() {
  const [saveScore, setSaveScore] = useState(false);
  const [scoreIsSaved, setScoreIsSaved] = useState(false);
  const dispatch = useDispatch();
  const questionNumber = useSelector((state) => state.quiz.quizQuestionNumber);
  const moneyEarned = useSelector((state) => state.quiz.moneyEarned);
  const isHighScore = useSelector((state) => state.bestResults.isHighScore);
  const gameOver = useSelector((state) => state.quiz.gameOver);
  const quizCompleted = useSelector((state) => state.quiz.quizCompleted);
  const userName = useRef();

  const handleBestResults = () => {
    dispatch(getBestResults());
    dispatch(toggleBestResults());
  };

  const handleSaveUserScore = () => {
    const userScore = {
      userName: userName.current.value,
      questionsAnswered: questionNumber - 1,
      moneyEarned: moneyEarned,
    };
    dispatch(saveUserScore(userScore));
    handleBestResults();
    setSaveScore(false);
    setScoreIsSaved(true);
  };

  return saveScore ? (
    <div className="save-score">
      <h2>User Name</h2>
      <input className=" form-control" ref={userName} />
      <button className="button" onClick={handleSaveUserScore}>
        Save
      </button>
    </div>
  ) : gameOver ? (
    <div className="game-over">
      <div className="game-over-header">
        <h1>GAME OVER</h1>
        <p>
          You answered {questionNumber - 1} questions and won ${moneyEarned}
        </p>
      </div>
      <button className="button" onClick={() => dispatch(endQuiz())}>
        Play again
      </button>
      <button className="button" onClick={() => handleBestResults()}>
        Best results
      </button>
      <button className={`button ${!isHighScore || scoreIsSaved ? "hidden" : ""}`} onClick={() => setSaveScore(true)}>
        Save your Score
      </button>
    </div>
  ) : quizCompleted ? (
    <div className="quiz-completed">
      <div className="quiz-completed-header">
        <h1>YOU WON!</h1>
        <p>
          You answered all {questionNumber} questions and won ${moneyEarned}
        </p>
        <h1>CONGRATULATIONS!</h1>
      </div>
      <button className="button" onClick={() => dispatch(endQuiz())}>
        Play again
      </button>
      <button className="button" onClick={() => handleBestResults()}>
        Best results
      </button>
      <button className={`button ${!isHighScore || scoreIsSaved ? "hidden" : ""}`} onClick={() => setSaveScore(true)}>
        Save your Score
      </button>
    </div>
  ) : (
    ""
  );
}
export default GameOver;
