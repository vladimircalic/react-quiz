import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { FaRegWindowClose } from "react-icons/fa";
import { toggleBestResults } from "../redux/bestResults/bestResultsActions";

function BestResults() {
  const bestResults = useSelector((state) => state.bestResults.bestResults);
  const dispatch = useDispatch();

  return (
    <div className="best-results-page">
      <h1>BEST RESULTS</h1>
      <FaRegWindowClose size={20} onClick={() => dispatch(toggleBestResults())} />
      <div className="best-results">
        {bestResults.map((result, index) => {
          return (
            <div key={result._id} className="result">
              <span className="user-name">
                {index + 1}. {result.userName}
              </span>
              <span>${result.moneyEarned}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default BestResults;
