import { combineReducers } from "redux";
import quizReducer from "./quiz/quizReducer";
import bestResultsReducer from "./bestResults/bestResultsReducer";

const rootReducer = combineReducers({
  quiz: quizReducer,
  bestResults: bestResultsReducer,
});

export default rootReducer;
