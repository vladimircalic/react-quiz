import {
  GET_BEST_RESULTS_REQUEST,
  GET_BEST_RESULTS_SUCCESS,
  GET_BEST_RESULTS_ERROR,
  TOGGLE_BEST_RESULTS,
  CHECK_HIGH_SCORE_REQUEST,
  CHECK_HIGH_SCORE_SUCCESS,
  CHECK_HIGH_SCORE_ERROR,
  SAVE_USER_SCORE_REQUEST,
  SAVE_USER_SCORE_SUCCESS,
  SAVE_USER_SCORE_ERROR,
} from "./bestResultsTypes";
import axios from "axios";

export const getBestResultsRequest = () => {
  return {
    type: GET_BEST_RESULTS_REQUEST,
  };
};

export const getBestResultsSuccess = (results) => {
  return {
    type: GET_BEST_RESULTS_SUCCESS,
    payload: results,
  };
};

export const getBestResultsError = (error) => {
  return {
    type: GET_BEST_RESULTS_ERROR,
    payload: error,
  };
};

export const getBestResults = () => {
  return (dispatch) => {
    dispatch(getBestResultsRequest());
    axios
      .get("/api/best-results")
      .then((res) => dispatch(getBestResultsSuccess(res.data)))
      .catch((err) => dispatch(getBestResultsError(err.message)));
  };
};

export const toggleBestResults = () => {
  return {
    type: TOGGLE_BEST_RESULTS,
  };
};

export const checkHighScoreRequest = () => {
  return {
    type: CHECK_HIGH_SCORE_REQUEST,
  };
};

export const checkHighScoreSuccess = (highScore) => {
  return {
    type: CHECK_HIGH_SCORE_SUCCESS,
    payload: highScore,
  };
};

export const checkHighScoreError = (error) => {
  return {
    type: CHECK_HIGH_SCORE_ERROR,
    payload: error,
  };
};

export const checkHighScore = (correctAnswers) => {
  return (dispatch) => {
    dispatch(checkHighScoreRequest());
    axios
      .get(`/api/best-results/${correctAnswers}`)
      .then((res) => dispatch(checkHighScoreSuccess(res.data)))
      .catch((err) => dispatch(checkHighScoreError(err.message)));
  };
};

export const saveUserScoreRequest = () => {
  return {
    type: SAVE_USER_SCORE_REQUEST,
  };
};

export const saveUserScoreSuccess = () => {
  return {
    type: SAVE_USER_SCORE_SUCCESS,
  };
};

export const saveUserScoreError = (error) => {
  return {
    type: SAVE_USER_SCORE_ERROR,
    payload: error,
  };
};

export const saveUserScore = (userScore) => {
  return (dispatch) => {
    dispatch(saveUserScoreRequest());
    axios
      .post("/api/best-results", userScore)
      .then((res) => dispatch(saveUserScoreSuccess(res.data)))
      .catch((err) => dispatch(saveUserScoreError(err.message)));
  };
};
