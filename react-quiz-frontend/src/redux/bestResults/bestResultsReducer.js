import {
  GET_BEST_RESULTS_REQUEST,
  GET_BEST_RESULTS_SUCCESS,
  GET_BEST_RESULTS_ERROR,
  TOGGLE_BEST_RESULTS,
  CHECK_HIGH_SCORE_REQUEST,
  CHECK_HIGH_SCORE_SUCCESS,
  CHECK_HIGH_SCORE_ERROR,
  SAVE_USER_SCORE_REQUEST,
  SAVE_USER_SCORE_SUCCESS,
  SAVE_USER_SCORE_ERROR,
} from "./bestResultsTypes";

const initialState = {
  loading: false,
  showBestResults: false,
  bestResults: [],
  isHighScore: false,
  error: "",
};

const bestResultsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BEST_RESULTS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_BEST_RESULTS_SUCCESS:
      return {
        ...state,
        loading: false,
        bestResults: action.payload,
      };
    case GET_BEST_RESULTS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case TOGGLE_BEST_RESULTS:
      return {
        ...state,
        showBestResults: !state.showBestResults,
      };
    case CHECK_HIGH_SCORE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CHECK_HIGH_SCORE_SUCCESS:
      return {
        ...state,
        loading: false,
        isHighScore: action.payload,
      };
    case CHECK_HIGH_SCORE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case SAVE_USER_SCORE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SAVE_USER_SCORE_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case SAVE_USER_SCORE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default bestResultsReducer;
