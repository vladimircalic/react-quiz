import {
  START_NEW_QUIZ,
  END_QUIZ,
  GET_QUESTION_REQUEST,
  GET_QUESTION_SUCCESS,
  GET_QUESTION_ERROR,
  GAME_OVER,
  HANDLE_MONEY_EARNED,
  CALL_FRIEND_HELPER,
  FIFTY_FIFTY_HELPER,
  SWITCH_QUESTION_REQUEST,
  SWITCH_QUESTION_ERROR,
  SWITCH_QUESTION_SUCCESS,
  QUIZ_COMPLETED,
  TOGGLE_PROGRESS_LIST,
} from "./quizTypes";
import axios from "axios";

export const startNewQuiz = () => {
  return {
    type: START_NEW_QUIZ,
  };
};

export const endQuiz = () => {
  return {
    type: END_QUIZ,
  };
};

export const gameOver = () => {
  return {
    type: GAME_OVER,
  };
};

export const getQuestionRequest = () => {
  return {
    type: GET_QUESTION_REQUEST,
  };
};

export const getQuestionSuccess = (question) => {
  return {
    type: GET_QUESTION_SUCCESS,
    payload: question,
  };
};

export const getQuestionError = (error) => {
  return {
    type: GET_QUESTION_ERROR,
    payload: error,
  };
};

export const getQuestion = (number) => {
  return (dispatch) => {
    dispatch(getQuestionRequest());
    axios
      .get(`/api/question/${number}`)
      .then((res) => dispatch(getQuestionSuccess(res.data)))
      .catch((err) => dispatch(getQuestionError(err.message)));
  };
};

export const handleMoneyEarned = (summ) => {
  return {
    type: HANDLE_MONEY_EARNED,
    payload: summ,
  };
};

export const callFriendHelper = (friendsAnswer) => {
  return {
    type: CALL_FRIEND_HELPER,
    payload: friendsAnswer,
  };
};

export const fiftyFiftyHelper = (filteredOptions) => {
  return {
    type: FIFTY_FIFTY_HELPER,
    payload: filteredOptions,
  };
};

export const switchQuestionRequest = () => {
  return {
    type: SWITCH_QUESTION_REQUEST,
  };
};

export const switchQuestionSuccess = (newQuestion) => {
  return {
    type: SWITCH_QUESTION_SUCCESS,
    payload: newQuestion,
  };
};

export const switchQuestionError = (error) => {
  return {
    type: SWITCH_QUESTION_ERROR,
    payload: error,
  };
};

export const switchQuestionHelper = (activeQuestion) => {
  return (dispatch) => {
    dispatch(switchQuestionRequest());
    axios
      .post("/api/switch-question/", activeQuestion)
      .then((res) => dispatch(switchQuestionSuccess(res.data)))
      .catch((err) => dispatch(switchQuestionError(err.message)));
  };
};

export const quizCompleted = () => {
  return {
    type: QUIZ_COMPLETED,
  };
};

export const toggleProgressList = () => {
  return {
    type: TOGGLE_PROGRESS_LIST,
  };
};
