import {
  START_NEW_QUIZ,
  END_QUIZ,
  GET_QUESTION_REQUEST,
  GET_QUESTION_SUCCESS,
  GET_QUESTION_ERROR,
  GAME_OVER,
  HANDLE_MONEY_EARNED,
  CALL_FRIEND_HELPER,
  FIFTY_FIFTY_HELPER,
  SWITCH_QUESTION_REQUEST,
  SWITCH_QUESTION_SUCCESS,
  SWITCH_QUESTION_ERROR,
  QUIZ_COMPLETED,
  TOGGLE_PROGRESS_LIST,
} from "./quizTypes";

const initialState = {
  loading: false,
  quizStarted: false,
  quizQuestionNumber: 0,
  questionHelpers: { callFriend: true, switchQuestion: true, fiftyFifty: true },
  showProgressList: false,
  question: "",
  moneyEarned: 0,
  quizCompleted: false,
  gameOver: false,
  error: "",
};

const quizReducer = (state = initialState, action) => {
  switch (action.type) {
    case START_NEW_QUIZ:
      return {
        ...state,
        quizStarted: true,
      };
    case END_QUIZ:
      return initialState;
    case GAME_OVER:
      return {
        ...state,
        showProgressList: false,
        gameOver: true,
      };
    case GET_QUESTION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_QUESTION_SUCCESS:
      return {
        ...state,
        loading: false,
        quizQuestionNumber: state.quizQuestionNumber + 1,
        question: action.payload,
      };
    case GET_QUESTION_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case HANDLE_MONEY_EARNED:
      return {
        ...state,
        moneyEarned: action.payload,
      };
    case CALL_FRIEND_HELPER:
      return {
        ...state,
        question: { ...state.question, friendsAnswer: action.payload },
        questionHelpers: { ...state.questionHelpers, callFriend: false },
      };
    case FIFTY_FIFTY_HELPER:
      return {
        ...state,
        question: { ...state.question, filteredOptions: action.payload },
        questionHelpers: { ...state.questionHelpers, fiftyFifty: false },
      };
    case SWITCH_QUESTION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SWITCH_QUESTION_SUCCESS:
      return {
        ...state,
        loading: false,
        question: action.payload,
        questionHelpers: { ...state.questionHelpers, switchQuestion: false },
      };
    case SWITCH_QUESTION_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case QUIZ_COMPLETED:
      return {
        ...state,
        quizCompleted: true,
      };
    case TOGGLE_PROGRESS_LIST:
      return {
        ...state,
        showProgressList: !state.showProgressList,
      };
    default:
      return state;
  }
};

export default quizReducer;
