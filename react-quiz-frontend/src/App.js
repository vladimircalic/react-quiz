import React from "react";
import "./App.scss";
import Menu from "./components/Menu";
import QuestionHelpers from "./components/QuestionHelpers";
import ProgressList from "./components/ProgressList";

function App() {
  return (
    <div className="App">
      <QuestionHelpers />
      <Menu />
      <ProgressList />
    </div>
  );
}

export default App;
