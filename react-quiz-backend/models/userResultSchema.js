const mongoose = require("mongoose");

const userResultSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: true,
  },
  questionsAnswered: {
    type: Number,
    required: true,
  },
  moneyEarned: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("UserResult", userResultSchema);
