const express = require("express");
const router = express.Router();
const Question = require("../models/questionSchema");

router.get("/question/:number", async (req, res, next) => {
  try {
    const questions = await Question.find({ questionNumber: req.params.number });
    const question = questions[Math.floor(Math.random() * questions.length)];
    res.status(200).send(question);
  } catch (err) {
    console.log(err);
  }
});

router.post("/switch-question", async (req, res, next) => {
  try {
    const questions = await Question.find({ questionNumber: req.body.questionNumber });
    const newQuestion = questions.filter((question) => {
      return question.question !== req.body.question;
    })[Math.floor(Math.random() * (questions.length - 1))];
    res.status(200).send(newQuestion);
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
