const express = require("express");
const router = express.Router();
const UserResult = require("../models/userResultSchema");

router.get("/best-results", async (req, res, next) => {
  try {
    const bestResults = await UserResult.find();
    bestResults
      .sort(function (a, b) {
        return a.questionsAnswered - b.questionsAnswered;
      })
      .reverse();
    res.status(200).send(bestResults);
  } catch (err) {
    console.log(err);
  }
});

router.get("/best-results/:correctAnswers", async (req, res, next) => {
  try {
    const bestResults = await UserResult.find();
    bestResults.sort(function (a, b) {
      return a.questionsAnswered - b.questionsAnswered;
    });
    if (bestResults[0].questionsAnswered >= req.params.correctAnswers && bestResults.length === 10) {
      res.status(200).send(false);
    } else {
      res.status(200).send(true);
    }
  } catch (err) {
    console.log(err);
  }
});

router.post("/best-results", async (req, res, next) => {
  try {
    const newHighScore = new UserResult(req.body);
    const bestResults = await UserResult.find();
    bestResults.sort(function (a, b) {
      return a.questionsAnswered - b.questionsAnswered;
    });
    if (bestResults.length === 10) bestResults[0].remove();

    newHighScore.save();
    res.status(200).send("Score saved!");
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
