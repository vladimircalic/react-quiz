const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const port = process.env.PORT || 5000;
const questionsRouter = require("./routes/question-routes");
const bestResultsRouter = require("./routes/bestResults-routes");

//Database connection
mongoose.connect("mongodb+srv://noliferop:svesuiste1@gymer-app.zscig.mongodb.net/React-Quiz", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => console.log("Connected to React-Quiz Database"));

app.use(express.json());
app.use(express.static(path.join(__dirname, "../react-quiz-frontend/build")));
app.get("/", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../react-quiz-frontend/build/") });
});
//Application routes
app.use("/api", questionsRouter);
app.use("/api", bestResultsRouter);
//Invalid routes
app.use((req, res, next) => {
  res.status(404).send("Not found");
});

//Connect app
app.listen(port, () => console.log(`React-Quiz listening on port  ${port}`));
